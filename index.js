var express = require('express');
var bodyParser = require('body-parser');
var app = express();
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(express.static('public'));
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});
app.post('/soms/getlogin', function (req, res) {
  
  var body = JSON.stringify(req.body, null, 2)
  body = JSON.parse(body)
  console.dir(body)
  
  var respuesta = {
    SOMAS01WOperationResponse: {
      ca_input: {
        ca_usuario: 'ADMSOMS1',
        ca_password: ''
      },
      ca_output: {
        ca_nivel: '09',
        ca_loc_usuario: 2,
        ca_ult_acceso: '2019-02-25'
      },
      ca_resp: {
        ca_cod: '000',
        ca_msg: 'LOGIN CORRECTO      '
      }
    }
  }
  res.send(respuesta);
});

app.post('/soms/getcte', function (req, res) {
  var body = JSON.stringify(req.body, null, 2)
  body = JSON.parse(body)
  console.dir(body)
  var respuesta =
  {
    SOMAV07WOperationResponse: {
      input_data: {
        lada_cte: '055',
        tel_cte: '58071621',
        nombre: '',
        calle: '',
        colonia: '',
        cod_postal: '',
        edo_rep: ''
      },
      output_resp: {
        codigo: '000',
        descripcion: 'CLIENTE ENCONTRADO EN SOMS'
      },
      output_data: {
        cliente: [
          {
            id_cliente: '0001923351',
            id_direccion: '',
            nombre: 'ERNESTO GONZALEZ DE LA CRUZ',
            estatus: 'ACT',
            calle: '',
            estado: '',
            cp: '',
            multdir: 'S',
            telefono: '5558071621',
            id_colonia: '',
            id_estado: '',
            id_municipio: '',
            id_calle: '',
            num_ext: '',
            num_int: ''
          },
          {
            id_cliente: '0001231123',
            id_direccion: '123',
            nombre: 'JESUS GARCIA',
            estatus: 'ACT',
            calle: 'INDEPENDENCIA',
            estado: 'MEX',
            cp: '01180',
            multdir: 'N',
            telefono: '05558765422',
            id_colonia: '',
            id_estado: '',
            id_municipio: '',
            id_calle: '',
            num_ext: '122',
            num_int: ''
          },
          {
            id_cliente: '',
            id_direccion: '',
            nombre: '',
            estatus: '',
            calle: '',
            estado: '',
            cp: '',
            multdir: '',
            telefono: '',
            id_colonia: '',
            id_estado: '',
            id_municipio: '',
            id_calle: '',
            num_ext: '',
            num_int: ''
          },
          {
            id_cliente: '',
            id_direccion: '',
            nombre: '',
            estatus: '',
            calle: '',
            estado: '',
            cp: '',
            multdir: '',
            telefono: '',
            id_colonia: '',
            id_estado: '',
            id_municipio: '',
            id_calle: '',
            num_ext: '',
            num_int: ''
          },
          {
            id_cliente: '',
            id_direccion: '',
            nombre: '',
            estatus: '',
            calle: '',
            estado: '',
            cp: '',
            multdir: '',
            telefono: '',
            id_colonia: '',
            id_estado: '',
            id_municipio: '',
            id_calle: '',
            num_ext: '',
            num_int: ''
          },
          {
            id_cliente: '',
            id_direccion: '',
            nombre: '',
            estatus: '',
            calle: '',
            estado: '',
            cp: '',
            multdir: '',
            telefono: '',
            id_colonia: '',
            id_estado: '',
            id_municipio: '',
            id_calle: '',
            num_ext: '',
            num_int: ''
          },
          {
            id_cliente: '',
            id_direccion: '',
            nombre: '',
            estatus: '',
            calle: '',
            estado: '',
            cp: '',
            multdir: '',
            telefono: '',
            id_colonia: '',
            id_estado: '',
            id_municipio: '',
            id_calle: '',
            num_ext: '',
            num_int: ''
          },
          {
            id_cliente: '',
            id_direccion: '',
            nombre: '',
            estatus: '',
            calle: '',
            estado: '',
            cp: '',
            multdir: '',
            telefono: '',
            id_colonia: '',
            id_estado: '',
            id_municipio: '',
            id_calle: '',
            num_ext: '',
            num_int: ''
          },
          {
            id_cliente: '',
            id_direccion: '',
            nombre: '',
            estatus: '',
            calle: '',
            estado: '',
            cp: '',
            multdir: '',
            telefono: '',
            id_colonia: '',
            id_estado: '',
            id_municipio: '',
            id_calle: '',
            num_ext: '',
            num_int: ''
          },
          {
            id_cliente: '',
            id_direccion: '',
            nombre: '',
            estatus: '',
            calle: '',
            estado: '',
            cp: '',
            multdir: '',
            telefono: '',
            id_colonia: '',
            id_estado: '',
            id_municipio: '',
            id_calle: '',
            num_ext: '',
            num_int: ''
          }
        ]
      }
    }
  }

  res.send(respuesta);
});
app.post('/soms/getctedir', function (req, res) {
  var body = JSON.stringify(req.body, null, 2)
  body = JSON.parse(body)
  console.dir(body)
  var respuesta =
  {
    SOMAV08WOperationResponse: {
      app_req: {
        cte_id_cliente: '0001923351',
        cte_calle: '',
        cte_colonia: '',
        cte_cp: '',
        cte_estado: '',
        cte_mdm: {
          mdm_id_colonia: '',
          mdm_id_estado: '',
          mdm_id_municipio: '',
          mdm_id_calle: '',
          mdm_id_num_ext: '',
          mdm_id_num_int: ''
        }
      },
      app_res: {
        cte_respuesta: {
          res_codigo: '000',
          res_descripcion: ''
        },
        cte_direccion: [
          {
            dir_id_direccion: '001',
            dir_calle: 'JACARANDAS',
            dir_numero: '129B',
            dir_colonia: 'SANTA MARIA TOTOLTEPEC',
            dir_estado: '',
            dir_cp: '050245'
          },
          {
            dir_id_direccion: '002',
            dir_calle: 'JACARANDAS',
            dir_numero: '129B',
            dir_colonia: 'SANTA MARIA TOTOLTEPEC',
            dir_estado: 'MEX',
            dir_cp: '050245'
          },
          {
            dir_id_direccion: '003',
            dir_calle: 'JACARANDAS',
            dir_numero: '129B',
            dir_colonia: 'SANTA MARIA TOTOLTEPEC',
            dir_estado: 'MEX',
            dir_cp: '050245'
          },
          {
            dir_id_direccion: '004',
            dir_calle: 'JACARANDAS',
            dir_numero: '129B',
            dir_colonia: 'SANTA MARIA TOTOLTEPEC',
            dir_estado: 'MEX',
            dir_cp: '050245'
          },
          {
            dir_id_direccion: '005',
            dir_calle: 'JACARANDAS',
            dir_numero: '129B',
            dir_colonia: 'SANTA MARIA TOTOLTEPEC',
            dir_estado: 'MEX',
            dir_cp: '050245'
          },
          {
            dir_id_direccion: '006',
            dir_calle: 'JACARANDAS',
            dir_numero: '129B',
            dir_colonia: 'SANTA MARIA TOTOLTEPEC',
            dir_estado: 'MEX',
            dir_cp: '050245'
          },
          {
            dir_id_direccion: '007',
            dir_calle: 'JACARANDAS',
            dir_numero: '129B',
            dir_colonia: 'SANTA MARIA TOTOLTEPEC',
            dir_estado: 'MEX',
            dir_cp: '050245'
          },
          {
            dir_id_direccion: '',
            dir_calle: '',
            dir_numero: '',
            dir_colonia: '',
            dir_estado: '',
            dir_cp: ''
          },
          {
            dir_id_direccion: '',
            dir_calle: '',
            dir_numero: '',
            dir_colonia: '',
            dir_estado: '',
            dir_cp: ''
          },
          {
            dir_id_direccion: '',
            dir_calle: '',
            dir_numero: '',
            dir_colonia: '',
            dir_estado: '',
            dir_cp: ''
          }
        ]
      }
    }
  }

  res.send(respuesta);
});
app.post("/soms/ConsultaOV", function(req, res) {
  var body = JSON.stringify(req.body, null, 2);
  body = JSON.parse(body);
  console.dir(body);
  var respuesta =
  {
  }

  res.send(respuesta);
});
app.post('/soms/AltaOrdenWeb', function (req, res) {
  var body = JSON.stringify(req.body, null, 2)
  body = JSON.parse(body)
  console.dir(body)
  var respuesta =
  {
    SMWEB00POperationResponse: {
      message_response: {
        returncode: 0,
        ordenId: '9000000001',
        tipoOrden: '02',
        locacion: '0004',
        vendedor: '12312312',
        fechaEmision: '12\/12\/2019',
        tipoPago: '02',
        intento: 1,
        estado: 'CONF',
        boleta: 1233,
        importeTotal: 12333,
        usuarioId: 'ADMSOMS1',
        transportista: 'TRANSPORTISTA',
        observaciones: 'NINGUNA',
        locacionVenta: 9,
        lada_ord: '055',
        telefono_ord: '99888888',
        destinatario: {
          nombre: 'PRUEBA TEST',
          calle: 'JACARANDA',
          asentamiento: 'TOLUCA',
          cp: '001180',
          lada: '055',
          telefono: '12312323'
        },
        remitente: {
          nombre: 'PRUEBA TEST',
          calle: 'JACARANDA',
          asentamiento: 'TOLUCA',
          cp: '001180',
          lada: '055',
          telefono: '12312323'
        }
      }
    }
  }

  res.send(respuesta);
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});