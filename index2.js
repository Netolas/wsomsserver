var express = require("express");
var request = require("request");
var bodyParser = require("body-parser");
var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static("public"));
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});
app.post("/soms/getlogin", function(req, res) {
  var body = JSON.stringify(req.body, null, 2);
  body = JSON.parse(body);
  console.dir(body);
  request.post(
    {
      uri: "http://172.17.210.36:3513/soms/getlogin",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(req.body)
    },
    function(error, response, body) {
      res.send(body);
    }
  );
});

app.post("/soms/getcte", function(req, res) {
  var body = JSON.stringify(req.body, null, 2);
  body = JSON.parse(body);
  console.dir(body);
  request.post(
    {
      uri: "http://172.17.210.36:3513/soms/getcte",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(req.body)
    },
    function(error, response, body) {
      //Print the Response
      res.send(body);
    }
  );
});
app.post("/soms/getctedir", function(req, res) {
  var body = JSON.stringify(req.body, null, 2);
  body = JSON.parse(body);
  console.dir(body);
  request.post(
    {
      uri: "http://172.17.210.36:3513/soms/getctedir",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(req.body)
    },
    function(error, response, body) {
      //Print the Response
      res.send(body);
    }
  );
});
app.post("/soms/AltaOrdenWeb", function(req, res) {
  var body = JSON.stringify(req.body, null, 2);
  body = JSON.parse(body);
  console.dir(body);
  request.post(
    {
      uri: "http://172.17.210.36:3513/soms/AltaOrdenWeb",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(req.body)
    },
    function(error, response, body) {
      //Print the Response
      res.send(body);
    }
  );
});
app.post("/soms/ConsultaOV", function(req, res) {
  var body = JSON.stringify(req.body, null, 2);
  body = JSON.parse(body);
  console.dir(body);
  request.post(
    {
      uri: "http://172.17.210.36:3513/soms/ConsultaOV",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(req.body)
    },
    function(error, response, body) {
      //Print the Response
      res.send(body);
    }
  );
});

app.listen(3000, function() {
  console.log("DEMO SOMS corriendo en puerto 3000!");
});